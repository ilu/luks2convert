#!/bin/bash

echo "This tool is meant to be used with freshly installed systems that do not hold any user data."
echo "If something goes wrong you WILL lose all your data. Backup first! Use this at your own risk."
echo "Syntax: luks2_convert <luksdevice>"
 
PASSWD="$1"
SKIPDEV="$2"

if [ -z "$PASSWD" ]; then
    echo "No LUKS device or password provided - exiting"
    exit 1
fi

# Be root to run this script
if [ $EUID -ne 0 ]; then
  echo "Please, type root password..."
  su -c "$0 $@"
  exit
fi

# Loop all partitions
for DEV in $(lsblk -lpo NAME,FSTYPE | grep crypto | awk '{print $1}'); do
    # Check if partition kan be skipped
    if [ "$DEV" == "$SKIPDEV" ]; then
        continue
    fi
    
    # Save header
    HEADER=$(cryptsetup luksDump "$DEV" | egrep '(Version:|PBKDF:)')
    
    if [ -z "$HEADER" ]; then
        echo "$DEV: not a LUKS device"
        continue
    fi

    # Check for LUKS2
    if [[ "$HEADER" =~ Version:[[:space:]]*2 ]]; then
        # Check for argon2id
        if [[ "$HEADER" == *argon2id* ]]; then
            echo "$DEV: LUKS2 with argon2id - nothing to do"
        else
            read -n 1 -p "Convert $DEV to LUKS2/argon2id (y/N)? " answer
            case $answer in
                y|Y )
                    echo "$PASSWD" | cryptsetup luksConvertKey --pbkdf argon2id "$DEV"
                    echo -e "\nLUKS2 info for $DEV:\n"
                    cryptsetup luksDump "$DEV"
                    echo
                    ;;
                * )
                    ;;
            esac
            
        fi
    else
        read -n 1 -p "Convert $DEV from LUKS1 to LUKS2/argon2id (y/N)? " answer
        case $answer in
            y|Y )
                cryptsetup convert -q --type luks2 "$DEV"
                echo "$PASSWD" | cryptsetup luksConvertKey --pbkdf argon2id "$DEV"
                echo -e "\nLUKS2 info for $DEV:\n"
                cryptsetup luksDump "$DEV"
                echo
                ;;
            * )
                ;;
        esac
    fi
    echo
done

exit 0

