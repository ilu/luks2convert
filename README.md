# Luks2Convert

bash script to convert luks encryption to luks2 with latest argon2id key derivation function. 

## Use at your own risk and don't forget to backup your key and your data first!

Please use with care and only if you understand what the script is doing! Backup your luks key first onto an external storage media. This script changes your storage device and might destroy your data if something goes wrong.

## Usage
You cannot convert a system partition while it is running. Boot a live system and run the script from there. 

## Contributing

Open an issue if you find bugs or have ideas for improvement.

## Authors and acknowledgment
Made by https://gitlab.com/hhabermann and https://github.com/abalfoort for SolydXK

## License
EUPL-1.2 (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

